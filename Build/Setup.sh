#! /bin/bash
#   Copyright 2012 Evolution
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

errors () {
	echo
	echo -e "\033[00;31m    Error:\033[0m failed to compile apk!"
	cd $base
	rm -fr $finished/*.exe $finished/apktool.jar $finished/out out
	sleep 6
	evolution
}
error2 () {
	echo
	echo -e "\033[00;31m    Error:\033[0m failed to decompile apk!"
	cd $base
	rm -fr $finished/*.exe $finished/apktool.jar $finished/out
	sleep 6
	evolution

}

after () {
	echo
	echo "        java -Xmx512m -jar apktool.jar b -f out $unsigned"
	echo "        -> signing apk"
	echo
	echo "    If you tested to so if the newly compiled apk works or not, what would"
	echo "    like to do."
	echo
	echo -e "        \033[01;36ma\033[0m - Keep the original"
	echo -e "        \033[01;36mb\033[0m - Replace the original"
	echo
	echo
	echo -en "\033[01;36m$USER@`hostname`\033[0m \033[01;33m~/Evolution $\033[0m "
	read keep
}
change () {
	echo
	echo "    Make any changes that you wish and press enter to compile and sign."
}

header () {
	clear
	echo -e "------------------------------------\033[01;36mEvolution\033[0m $VERSION-----------------------------------------"
	echo
	echo "   By: Earthnfire78 @ www.androidcentral.com                   2012evolutionbyor@gmail.com"
	echo -e "\033[01;33m *******************************************************************************************\033[0m"
	echo
	echo
	echo "    After apk is compile you well then be taken to the adb/fastboot menu, In order to"
    echo "    test if the newly compiled app works by pushing it to you phone.  The newly compile"
	echo "    app well be in the out directory."
}

decom_com () {
	if [ "$filechosen" == "framework-res.apk" ]; then
		unsigned=../$unsigned
		finished=Frameworks/Core/Res
	fi
	if [ "$filechosen" == "SystemUI.apk" ]; then
		finished=Frameworks/Base
	fi
	header
	echo
	echo "        java -Xmx512m -jar apktool.jar d -f -t 1-evoltion.apk, $filechosen out"
	java -Xmx512m -jar apktool.jar -q d -f $filechosen out
	if [ -d out ]; then
		change
		echo -n ""
		read enter
		if [ "$enter" == "" ]; then
			echo "        java -Xmx512m -jar apktool.jar b -f out $unsigned"
			java -Xmx512m -jar apktool.jar -q b -f out $unsigned
			cd $base
			if [ -d out ]; then
				cd out
				if [ -e unsigned.apk ]; then
					echo "        -> signing apk"
					cd ..
					java -Xmx512m -jar $signapk -w $platform.x509.pem $platform.pk8 out/unsigned.apk out/$filechosen
					rm -f out/unsigned.apk
					Build/Scripts/Adb_Fastboot
					header
					echo
					echo "        java -Xmx512m -jar apktool.jar d -f -t 1-evoltion.apk, $filechosen out"
					change
					after
					if [ "$keep" == "a" ]; then
						rm -fr $finished/*.exe $finished/apktool.jar $finished/out out
						sleep 3
						evolution
					elif [ "$keep" == "b" ]; then
						mv -f out/$filechosen $finished/$filechosen
						rm -fr $finished/*.exe $finished/apktool.jar $finished/out out
						sleep 3
						evolution
					fi
				else
					errors
				fi
			else
				errors
			fi
		fi
	else
		error2
	fi
}

leave () {
	clear
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo -e "                             \033[01;36m  eeeee x   x iiiii ttttt iiiii nn  n ggggg\033[0m"
	echo -e "                             \033[01;36m  e      x x    i     t     i   nn  n g   \033[0m"
	echo -e "                             \033[01;36m  eee    x x    i     t     i   n  nn g  gg\033[0m"
	echo -e "                             \033[01;36m  eeeee x   x iiiii   tt  iiiii n  nn ggggg\033[0m"
	echo
	mkdir -p Build/Tools/Backup
		if [ -e Build/Tools/Backup ]; then
			zip -r -y -q BackUp-`date '+%m%d%y-%H%M%S'` \
				* -x *.cvs* *.git* *.svn* \
				Build/Tools/Backup/*.zip*
			mv -f *.zip Build/Tools/Backup
		fi
	exit 1
	
}

evolution () {
	apktool=`echo Build/Tools/Apktool/apktool.jar Build/Tools/Apktool/aapt.exe`
	unsigned=../../out/unsigned.apk
	platform=Build/Keys/platform
	signapk=Build/Tools/Signapk/signapk.jar
	finished=Packages/Apps
	base=`pwd`
	clear
	echo -e "------------------------------------\033[01;36mEvolution\033[0m $VERSION-----------------------------------------"
	echo
	echo "   By: Earthnfire78 @ www.androidcentral.com                   2012evolutionbyor@gmail.com"
	echo -e "\033[01;33m *******************************************************************************************\033[0m"
	echo
	echo
	echo "    Theme Your ROM                                  Boot.img tools"
	echo
	echo -e "\033[01;36m        1\033[0m - framework-res                               \033[01;36m  39\033[0m - Convert .PNG into .RLE"
	echo -e "\033[01;36m        2\033[0m - SystemUI"
	echo -e "\033[01;36m        3\033[0m - Settings"
	echo -e "\033[01;36m        4\033[0m - Launchers"
	echo -e "\033[01;36m        5\033[0m - Themes (i.e Cyanbread/others)"
	echo
	echo "    Decompile different system apk                   Other tools"
	echo
	echo -e "\033[01;36m        6\033[0m - Mms                                         \033[01;36m  61\033[0m - Change ROM'S Name"
	echo -e "\033[01;36m        7\033[0m - Phone                                       \033[01;36m  62\033[0m - Sign apk/zip File"
	echo -e "\033[01;36m        8\033[0m - Contacts                                    \033[01;36m  63\033[0m - Port ROM (experimental)"
	echo -e "\033[01;36m        9\033[0m - Browser                                     \033[01;36m  64\033[0m - Adb/Fastboot"
	echo -e "\033[01;36m       10\033[0m - Other (\033[00;31mNOTE:\033[0m this may break the apk)"
	echo
	echo "    Build Options                                    Help and Support"
	echo
	echo -e "\033[01;36m       81\033[0m - LS670                                       \033[01;36m   0\033[0m - About/Help and Support"
	echo -e "\033[01;36m       82\033[0m - VM670"
	echo
	echo -e "\033[01;36m        x\033[0m - Exit Terminal"
	echo
	echo
    echo -en "\033[01;36m$USER@`hostname`\033[0m \033[01;33m~/Evolution $\033[0m "
    read build
	case "$build" in
		1)		echo; filechosen=framework-res.apk; cp -f $apktool Frameworks/Core/Res; cd Frameworks/Core/Res; decom_com ;;
		2)  	echo; filechosen=SystemUI.apk; cp -f $apktool Frameworks/Base; cd Frameworks/Base; decom_com ;;
		3) 		echo; filechosen=Settings.apk; cp -f $apktool $finished; cd $finished; decom_com ;;
		4)		echo; Build/Scripts/Launchers ;;
		5)  	echo; Build/Scripts/Theming ;;
		6)		echo; filechosen=Mms.apk; cp -f $apktool $finished; cd $finished; decom_com ;;
		7)		echo; filechosen=Phone.apk; cp -f $apktool $finished; cd $finished; decom_com ;;
		8)		echo; filechosen=Contacts.apk; cp -f $apktool $finished; cd $finished; decom_com ;;
		9)   	echo; filechosen=Browser.apk; cp -f $apktool $finished; cd $finished; decom_com ;;
		10)		echo; Build/Scripts/Other ;;
		38)		echo; Build/Scripts/Unpack ;;
		39)		echo; Build/Scripts/Bootimages ;;
		40)		echo; Build/Scripts/Pack ;;
		61)		echo; Build/Scripts/Naming ;;
		62)		echo; Build/Scripts/Signing ;;
		63)		echo; Build/Scripts/Porting ;;
		64)		echo; Build/Scripts/Adb_Fastboot ;;
		81)		echo; Build/Scripts/Sprint ;;
		82)		echo; Build/Scripts/VirginMobile ;;
		0)		echo; Build/Scripts/Support ;;
		x)  	echo; leave;;
		 *)
			echo; echo "    Please chose one of the above options"; sleep 3
		;;
	esac
}

# Here I'm going to check for the exacutables need for that tasks
# and job at hand.
if [ `uname | grep CYGWIN` ]; then
	PATH="$PATH:$PWD/other"
	export PATH
	#echo $PATH
	# Test for needed programs and warn if missing
	ERROR="0"
	show_header=1
	for PROGRAM in "java" "zip" "git" "gcc" "mintty" "cpio" "clear" "sed" "wget" "unzip" "cp" "mv" "mkdir" "chmod"
	do
		for (( i = 0 ; i < ${#PATH[@]} ; i++ ))
		do
			if [ $show_header == 1 ]; then
				mkdir -p Build/Tools/Backup
				clear
				echo
				echo
				echo
				echo
				echo
				echo
				echo
				echo
				echo
				echo
				echo -e "                     \033[01;36m  eeeee v   v   ooo  l     u   u ttttt  iiiii  ooo  nn  n\033[0m"
				echo -e "                     \033[01;36m  e      v v   o   o l     u   u   t      i   o   o nn  n\033[0m"
				echo -e "                     \033[01;36m  eee    v v   o   o l     u   u   t      i   o   o n  nn\033[0m"
				echo -e "                     \033[01;36m  eeeee  vvv    ooo  lllll  uuuu   tt   iiiii  ooo  n  nn\033[0m"
				echo
				echo -n "                             "
				for (( count = 0 ; $count <= $i ; count++ ))
				do
					echo -n " -"
				done
				chmod 755 Build/Tools/Apktool/* Build/Tools/Bootimg/* \
					Build/Tools/Platform-Tools/* Build/Tools/Signapk/* Build/Tools/Unyaffs/* \
					Build/Tools/Zipalign/* Build/Scripts/* Build/Scripts/*/*.sh
				cp -f Build/Tools/Apktool/aapt.exe \
					Build/Tools/Apktool/apktool.jar Frameworks/Core/Res
				cd Frameworks/Core/Res
				java -Xmx512m -jar apktool.jar -q if *.apk evoltion
				cd ../../../
				rm -f Frameworks/Core/Res/*.exe Frameworks/Core/Res/*.jar
				rm -f README.*
				# Since git well not add emplty directories, well have to creat them.
				mkdir -p Bootable/boot.img-ramdisk/data Bootable/boot.img-ramdisk/dev \
					Bootable/boot.img-ramdisk/proc Bootable/boot.img-ramdisk/sys \
					Bootable/boot.img-ramdisk/system
				show_header=0
			else
				echo -n "  -"
			fi
			which "$PROGRAM" > /dev/null 
			if [ "x$?" = "x1" ] ; then
				ERROR="1"				
				echo
				echo -e "  \033[00;31mERROR:\033[0m The program $PROGRAM is missing or is not in your PATH,"
				echo "         please install it or fix your PATH variable"
				sleep 10
			fi
		done
	done
	if [ "x$ERROR" = "x1" ] ; then
		exit 1
	fi
	# chmod all exacutables in Build directory
	while [ "1" = "1" ] ;
	do
		VERSION=V1.3.1; evolution
	done
	exit 0
else
	echo "Operating System `uname` not supported"
	sleep 4
	exit 0
fi
exit 0
